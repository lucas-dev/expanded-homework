package com.example.lucas.expandedhomework.orderhistory;

import com.example.lucas.expandedhomework.http.OrderHistoryApiService;
import com.example.lucas.expandedhomework.http.apimodel.OrderHistory;

import rx.Observable;

/**
 * Created by ls.godoy on 1/22/18.
 */

public class OrderHistoryRepository implements Repository {
    private OrderHistoryApiService orderHistoryApiService;

    public OrderHistoryRepository(OrderHistoryApiService orderHistoryApiService) {
        this.orderHistoryApiService = orderHistoryApiService;

    }


    @Override
    public Observable<OrderHistory> getOrderHistoryFromNetwork() {
        return orderHistoryApiService.getOrderHistory();
    }
}
