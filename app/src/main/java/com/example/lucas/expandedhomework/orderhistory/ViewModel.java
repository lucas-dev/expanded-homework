package com.example.lucas.expandedhomework.orderhistory;

import com.example.lucas.expandedhomework.http.apimodel.OrderHistory;

/**
 * Created by ls.godoy on 1/22/18.
 */

public class ViewModel {
    private OrderHistory orderHistory;
//    private String year;
//    private String title;
//    private String date;
//    private String status;
//
//    public ViewModel(String year, String title, String date, String status) {
//        this.year = year;
//        this.title = title;
//        this.date = date;
//        this.status = status;
//    }
//
//    public String getYear() {
//        return year;
//    }
//
//    public void setYear(String year) {
//        this.year = year;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }

    public ViewModel(OrderHistory orderHistory) {
        this.orderHistory = orderHistory;
    }

    public void setOrderHistory(OrderHistory orderHistory) {
        this.orderHistory = orderHistory;
    }

    public OrderHistory getOrderHistory() {
        return orderHistory;
    }
}
