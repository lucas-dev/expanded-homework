package com.example.lucas.expandedhomework.http.apimodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItem {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("orderId")
@Expose
private Integer orderId;
@SerializedName("categoryId")
@Expose
private Integer categoryId;
@SerializedName("price")
@Expose
private Double price;
@SerializedName("attributes")
@Expose
private List<String> attributes = null;
@SerializedName("imageUrl")
@Expose
private String imageUrl;
@SerializedName("name")
@Expose
private String name;
@SerializedName("status")
@Expose
private String status;
@SerializedName("quantity")
@Expose
private Integer quantity;
@SerializedName("isDigitallyDelivered")
@Expose
private Boolean isDigitallyDelivered;
@SerializedName("shippingCarrier")
@Expose
private Object shippingCarrier;
@SerializedName("eGiftCodes")
@Expose
private List<String> eGiftCodes = null;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getOrderId() {
return orderId;
}

public void setOrderId(Integer orderId) {
this.orderId = orderId;
}

public Integer getCategoryId() {
return categoryId;
}

public void setCategoryId(Integer categoryId) {
this.categoryId = categoryId;
}

public Double getPrice() {
return price;
}

public void setPrice(Double price) {
this.price = price;
}

public List<String> getAttributes() {
return attributes;
}

public void setAttributes(List<String> attributes) {
this.attributes = attributes;
}

public String getImageUrl() {
return imageUrl;
}

public void setImageUrl(String imageUrl) {
this.imageUrl = imageUrl;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Integer getQuantity() {
return quantity;
}

public void setQuantity(Integer quantity) {
this.quantity = quantity;
}

public Boolean getIsDigitallyDelivered() {
return isDigitallyDelivered;
}

public void setIsDigitallyDelivered(Boolean isDigitallyDelivered) {
this.isDigitallyDelivered = isDigitallyDelivered;
}

public Object getShippingCarrier() {
return shippingCarrier;
}

public void setShippingCarrier(Object shippingCarrier) {
this.shippingCarrier = shippingCarrier;
}

public List<String> getEGiftCodes() {
return eGiftCodes;
}

public void setEGiftCodes(List<String> eGiftCodes) {
this.eGiftCodes = eGiftCodes;
}

}