package com.example.lucas.expandedhomework.http.apimodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("orderDate")
@Expose
private String orderDate;
@SerializedName("shippingAddress")
@Expose
private Object shippingAddress;
@SerializedName("orderItems")
@Expose
private List<OrderItem> orderItems = null;
@SerializedName("totalPrice")
@Expose
private Double totalPrice;
@SerializedName("emailAddress")
@Expose
private Object emailAddress;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getOrderDate() {
return orderDate;
}

public void setOrderDate(String orderDate) {
this.orderDate = orderDate;
}

public Object getShippingAddress() {
return shippingAddress;
}

public void setShippingAddress(Object shippingAddress) {
this.shippingAddress = shippingAddress;
}

public List<OrderItem> getOrderItems() {
return orderItems;
}

public void setOrderItems(List<OrderItem> orderItems) {
this.orderItems = orderItems;
}

public Double getTotalPrice() {
return totalPrice;
}

public void setTotalPrice(Double totalPrice) {
this.totalPrice = totalPrice;
}

public Object getEmailAddress() {
return emailAddress;
}

public void setEmailAddress(Object emailAddress) {
this.emailAddress = emailAddress;
}

}