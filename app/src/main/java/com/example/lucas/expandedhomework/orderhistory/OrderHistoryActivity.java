package com.example.lucas.expandedhomework.orderhistory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lucas.expandedhomework.R;
import com.example.lucas.expandedhomework.http.apimodel.OrderHistory;
import com.example.lucas.expandedhomework.http.apimodel.OrderYear;
import com.example.lucas.expandedhomework.root.App;

import java.util.List;

import javax.inject.Inject;

public class OrderHistoryActivity extends AppCompatActivity implements OrderHistoryMVP.View {

    private final String TAG = "Manon";
    private RecyclerView mContainer;
    private OrderHistoryHeaderAdapter mAdapterYears;

    @Inject
    OrderHistoryMVP.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        ((App) getApplication()).getComponent().inject(this);

        initViews();


        mPresenter.setView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    private void initViews() {
        mContainer = (RecyclerView) findViewById(R.id.container);
        mContainer.setLayoutManager(new LinearLayoutManager(this));
        mAdapterYears = new OrderHistoryHeaderAdapter();
        mContainer.setAdapter(mAdapterYears);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.rxUnsubscribe();

    }

    @Override
    public void updateData(ViewModel viewModel) {
        List<OrderYear> orderYears = viewModel.getOrderHistory().getData();
        mAdapterYears.addAll(orderYears);
        mAdapterYears.notifyDataSetChanged();
        mAdapterYears.setOnItemClickListener(new OrderHistoryHeaderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(OrderYear orderYear) {
                Toast.makeText(OrderHistoryActivity.this, orderYear.getYear()+"", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void showSnackbar(String s) {

    }
}
