package com.example.lucas.expandedhomework.root;

import android.app.Application;

import com.example.lucas.expandedhomework.http.ApiModuleForOrderHistory;
import com.example.lucas.expandedhomework.orderhistory.OrderHistoryModule;

/**
 * Created by lucas on 21/01/18.
 */

public class App extends Application {
    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .apiModuleForOrderHistory(new ApiModuleForOrderHistory())
                .orderHistoryModule(new OrderHistoryModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
