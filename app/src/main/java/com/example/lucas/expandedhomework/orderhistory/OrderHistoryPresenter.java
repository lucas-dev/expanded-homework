package com.example.lucas.expandedhomework.orderhistory;

import rx.Observer;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ls.godoy on 1/22/18.
 */

public class OrderHistoryPresenter implements OrderHistoryMVP.Presenter {
    private OrderHistoryMVP.View view;
    private Subscription subscription = null;
    private OrderHistoryMVP.Model model;

    public OrderHistoryPresenter(OrderHistoryMVP.Model model) {
        this.model = model;
    }

    @Override
    public void loadData() {
        subscription = model
                .result()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (view != null) {
                            view.showSnackbar("Error getting order history");
                        }
                    }

                    @Override
                    public void onNext(ViewModel viewModel) {
                        if (view != null) {
                            view.updateData(viewModel);
                        }
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed())
                subscription.unsubscribe();
        }
    }

    @Override
    public void setView(OrderHistoryMVP.View view) {
        this.view = view;
    }
}
