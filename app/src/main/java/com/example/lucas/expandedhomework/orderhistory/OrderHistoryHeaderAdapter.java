package com.example.lucas.expandedhomework.orderhistory;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lucas.expandedhomework.R;
import com.example.lucas.expandedhomework.http.apimodel.Order;
import com.example.lucas.expandedhomework.http.apimodel.OrderItem;
import com.example.lucas.expandedhomework.http.apimodel.OrderYear;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucas on 27/01/18.
 */

public class OrderHistoryHeaderAdapter extends RecyclerView.Adapter<OrderHistoryHeaderAdapter.ViewHolder> {
    private List<OrderYear> mOrderYearList;

    private OnItemClickListener onItemClickListener;
    private boolean shouldBeOpened = true;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(OrderYear orderYear);
    }

    public OrderHistoryHeaderAdapter() {
        mOrderYearList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_header, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvTitle.setText(mOrderYearList.get(position).getYear() + " Orders");
        // show first row as expanded by default
        if (position == 0 && shouldBeOpened) {
            holder.rvItems.setVisibility(View.VISIBLE);
            shouldBeOpened = false;
        }

        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.rvItems.getVisibility() == View.GONE)
                    holder.rvItems.setVisibility(View.VISIBLE);
                else
                    holder.rvItems.setVisibility(View.GONE);
            }
        });


        List<OrderItem> orderItems = new ArrayList<>();
        for (Order order : mOrderYearList.get(position).getOrders()) {
            for (OrderItem orderItem : order.getOrderItems())
                orderItems.add(orderItem);
        }

        holder.rvItems.removeAllViews();

        for (final OrderItem orderItem : orderItems) {
            LayoutInflater layoutInflater = (LayoutInflater) holder.rvItems.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.order_history_list, null);
            holder.rvItems.addView(view);
            ((TextView)view.findViewById(R.id.tvOrderItemName)).setText(orderItem.getName());
            ((TextView)view.findViewById(R.id.tvOrderItemPrice)).setText(orderItem.getPrice()+"");
            ((TextView)view.findViewById(R.id.tvOrderItemStatus)).setText(orderItem.getStatus());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(holder.rvItems.getContext(), orderItem.getName()+" "+orderItem.getPrice()+" "+ orderItem.getStatus(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(holder.rvItems.getContext(), OrderHistoryDetailsActivity.class);
                    intent.putExtra(OrderHistoryDetailsActivity.DETAILS_NAME, orderItem.getName());
                    intent.putExtra(OrderHistoryDetailsActivity.DETAILS_PRICE, orderItem.getPrice()+"");
                    intent.putExtra(OrderHistoryDetailsActivity.DETAILS_STATUS, orderItem.getStatus());
                    holder.rvItems.getContext().startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mOrderYearList.size();
    }


    public void addAll(List<OrderYear> orderYears) {
        mOrderYearList.clear();
        mOrderYearList.addAll(orderYears);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvIcon, tvTitle;
        LinearLayout rvItems;
        public ViewHolder(View view) {
            super(view);
            tvIcon = (TextView) view.findViewById(R.id.order_history_header_button);
            tvTitle = (TextView) view.findViewById(R.id.order_history_header_title);
            rvItems = (LinearLayout) view.findViewById(R.id.order_history_header_recycler);
        }
    }

}
