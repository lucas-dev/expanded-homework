package com.example.lucas.expandedhomework.http.apimodel;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderYear {

    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = null;

    private long id = -1;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

}