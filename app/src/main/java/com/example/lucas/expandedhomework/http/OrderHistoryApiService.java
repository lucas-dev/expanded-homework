package com.example.lucas.expandedhomework.http;

import com.example.lucas.expandedhomework.http.apimodel.OrderHistory;



import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by lucas on 21/01/18.
 */

public interface OrderHistoryApiService {
    @GET("5a65208c2b00001c17f414ca")
    Observable<OrderHistory> getOrderHistory();
}
