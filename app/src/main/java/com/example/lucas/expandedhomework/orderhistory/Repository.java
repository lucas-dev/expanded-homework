package com.example.lucas.expandedhomework.orderhistory;

import com.example.lucas.expandedhomework.http.apimodel.OrderHistory;

import rx.Observable;

/**
 * Created by lucas on 21/01/18.
 */

public interface Repository {
    Observable<OrderHistory> getOrderHistoryFromNetwork();
}
