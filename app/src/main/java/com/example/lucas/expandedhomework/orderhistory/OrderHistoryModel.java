package com.example.lucas.expandedhomework.orderhistory;

import com.example.lucas.expandedhomework.http.apimodel.OrderHistory;

import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by ls.godoy on 1/22/18.
 */

public class OrderHistoryModel implements OrderHistoryMVP.Model {
    private Repository repository;

    public OrderHistoryModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ViewModel> result() {
        return repository.getOrderHistoryFromNetwork().flatMap(new Func1<OrderHistory, Observable<ViewModel>>() {
            @Override
            public Observable<ViewModel> call(OrderHistory orderHistory) {
                return Observable.just(new ViewModel(orderHistory));
            }
        });

    }
}
