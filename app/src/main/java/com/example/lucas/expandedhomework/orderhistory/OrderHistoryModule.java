package com.example.lucas.expandedhomework.orderhistory;

import com.example.lucas.expandedhomework.http.OrderHistoryApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ls.godoy on 1/22/18.
 */

@Module
public class OrderHistoryModule {

    @Provides
    public OrderHistoryMVP.Presenter provideOrderHistoryPresenter(OrderHistoryMVP.Model orderHistoryModel) {
        return new OrderHistoryPresenter(orderHistoryModel);
    }

    @Provides
    public OrderHistoryMVP.Model provideOrderHistoryModel(Repository repository) {
        return new OrderHistoryModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(OrderHistoryApiService orderHistoryApiService) {
        return new OrderHistoryRepository(orderHistoryApiService);
    }

}
