package com.example.lucas.expandedhomework.orderhistory;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.lucas.expandedhomework.R;

public class OrderHistoryDetailsActivity extends AppCompatActivity {
    public static final String DETAILS_NAME = "DETAILS_NAME";
    public static final String DETAILS_PRICE = "DETAILS_PRICE";
    public static final String DETAILS_STATUS = "DETAILS_STATUS";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_details);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra(DETAILS_NAME));

        ((TextView)findViewById(R.id.tvOrderItemDetailsName)).setText(getIntent().getStringExtra(DETAILS_NAME));
        ((TextView)findViewById(R.id.tvOrderItemDetailsPrice)).setText(getIntent().getStringExtra(DETAILS_PRICE));
        ((TextView)findViewById(R.id.tvOrderItemDetailsStatus)).setText(getIntent().getStringExtra(DETAILS_STATUS));

    }

}
