package com.example.lucas.expandedhomework.orderhistory;


import rx.Observable;

/**
 * Created by ls.godoy on 1/22/18.
 */

public interface OrderHistoryMVP {
    interface View {
        void updateData(ViewModel viewModel);
        void showSnackbar(String s);
    }

    interface Presenter {
        void loadData();
        void rxUnsubscribe();
        void setView(OrderHistoryMVP.View view);
    }

    interface Model {
        Observable<ViewModel> result();
    }
}
