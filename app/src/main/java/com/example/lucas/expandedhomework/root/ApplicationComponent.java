package com.example.lucas.expandedhomework.root;

import com.example.lucas.expandedhomework.orderhistory.OrderHistoryActivity;
import com.example.lucas.expandedhomework.http.ApiModuleForOrderHistory;
import com.example.lucas.expandedhomework.orderhistory.OrderHistoryModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lucas on 21/01/18.
 */

@Singleton
@Component(modules= {ApplicationModule.class, ApiModuleForOrderHistory.class, OrderHistoryModule.class})
public interface ApplicationComponent {
    void inject(OrderHistoryActivity target);
}
