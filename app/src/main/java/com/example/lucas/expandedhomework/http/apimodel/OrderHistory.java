package com.example.lucas.expandedhomework.http.apimodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistory {

@SerializedName("data")
@Expose
private List<OrderYear> data = null;
@SerializedName("error")
@Expose
private Object error;

public List<OrderYear> getData() {
return data;
}

public void setData(List<OrderYear> data) {
this.data = data;
}

public Object getError() {
return error;
}

public void setError(Object error) {
this.error = error;
}

}